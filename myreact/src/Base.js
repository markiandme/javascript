
import Appkaa from './Lesson5Task2';
import Something from "./Appkayesterday";
import Lesson5Task1 from './Lesson5Task1';
import Lesson5Task3Parent from './Lesson5Task3Parent';
import Lesson5Task3Children from './Lesson5Task3Children';
import Lesson6Task2 from './Lesson6Task2';
function App() {
    return (
        <>
        <Appkaa />
        <Something />
        <Lesson5Task1 />
        <Lesson5Task3Children />
        <Lesson5Task3Parent />
        <Lesson6Task2 />
        </>
    );
}
export default App
