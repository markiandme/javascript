import {useState} from "react";
import {useRef} from "react";
function Something() {
const [randomNumber, setRandomNumber] = useState();
const [color, setColor] = useState("");

function generateNumber() {
  let random = Math.floor(Math.random() * 101);
  setRandomNumber(random);
}

function generateRandomColor() {
  let fasdasdasdirst = Math.floor(Math.random() * 256);
  let second = Math.floor(Math.random() * 256);
  let third = Math.floor(Math.random() * 256);
  setColor(`rgb(${first}, ${second}, ${third})`);
}

return (
  <>
    <button onClick={generateNumber}>Генерація</button>
    <p>Твоє число : {randomNumber}</p>
    <button onClick={generateRandomColor}>Клік</button>
    <p style={{color }}>rgb текстік</p>
  </>
)
  }
  export default Something;