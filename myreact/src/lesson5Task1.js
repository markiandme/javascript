import { useRef } from "react";
import { useState } from "react";

function Lesson5Task1 () {
    let name = useRef();
    let lastName = useRef();
    let email = useRef();
    let [data, setData] = useState([]);
  
    function addToState () {
      let newName = name.current.value;
      let newLastName = lastName.current.value;
      let newEmail = email.current.value;
  
      let newData = {name: newName, lastName: newLastName, email: newEmail};
      setData([...data, newData]);
      console.log("Інофрмація:", data);
    };
    return (
      <>
        <input ref={name} placeholder="Імя :" />
        <input ref={lastName} placeholder="Фамілія :" />
        <input ref={email} placeholder="Імейл :" />
        <button onClick={addToState}>Add Data</button>
      </>
    );
  }
  export default Lesson5Task1;